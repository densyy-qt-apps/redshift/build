#!/bin/bash

name=redshift
dir_install=/opt/redshift
dir_desktop=/usr/share/applications

sudo pacman -S redshift --noconfirm

sudo mkdir -p $dir_install
sudo cp $name $dir_install/$name
sudo cp $name.png $dir_install/$name.png
sudo cp $name.desktop $dir_desktop/$name.desktop